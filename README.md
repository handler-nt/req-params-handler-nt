# ReqParamsHandler

Handler for request parameters types
 
## Installation

<code>$ npm install req-params-handler-nt --save </code>

## Documentation

- <code>isObjectID(value)</code><br>
	Check if value is an ObjectId valid

	
- <code>isNumber(value)</code><br>
	Check if value is a number
	