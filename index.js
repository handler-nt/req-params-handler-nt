const mongoose = require('mongoose');
const errorHandler = require('error-handler-nt');

function isObjectId(value)
{
  return (value && mongoose.Types.ObjectId.isValid(String(value)));
}

function isNumber(params, path)
{
  return (params[path] && typeof value === 'number');
}

function checkIsObjectId(req, res, next)
{
  if (req.params && req.params.id && isObjectId(req.params.id))
    return next();

  return errorHandler.createAndSend(res, 404, {message: "Element not found", type: "NOT_FOUND"});
}

module.exports =
{
  isObjectId,
  isNumber,
  checkIsObjectId
};
