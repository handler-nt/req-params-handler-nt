const assert = require('assert');
const lib = require('../index');


const params =
{
  ObjectIDVal: "5bd64deb929e04241f49f486",
  NumberVal: 42
};

describe('Check mongoose isObjectId', () =>
{
  it ("Should return true", () =>
  {
    assert.equal(lib.isObjectId(params.ObjectIDVal), true);
  });

  it("Should return false", () =>
  {
    assert.equal(lib.isObjectId(params.NumberVal), false);
  });

});
